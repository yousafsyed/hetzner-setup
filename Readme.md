
Add following to cloud init for kubernetes clusters ubuntu-18.04
```bash
#cloud-config
locale: en_GB.UTF-8

runcmd:
  - curl -s https://gitlab.com/yousafsyed/hetzner-setup/-/raw/master/setup.sh | bash

```