#!/bin/bash

sed -i 's/[#]*PermitRootLogin yes/PermitRootLogin prohibit-password/g' /etc/ssh/sshd_config
sed -i 's/[#]*PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config

systemctl restart sshd



apt-get update
apt-get install -y jq ufw fail2ban

ufw allow proto tcp from any to any port 22,80,443,6443

ufw -f enable

ufw allow from 10.0.0.0/16

ufw -f default deny incoming
ufw -f default allow outgoing